    /**
     * ApproveAbsencesFormController.js
     * This Service Edge UI component creates two buttons which simplify the process of approving and cancelling
     * AbsenceRequestMeter functionality. The approve button is only available when the status is new/pending. 
     * The cancel button is only available when the status is Approved.
     * 
     * Class:                   w6!ApproveAbsences/formApproveAbsencesController.js/
     * DLL:                     w6!ApproveAbsences/formApproveAbsences.html/
     * AdditionalParameters*:  {"ButtonApproveText":"Accept request",
                                "ButtonCancelText":"Cancel request",
                                "AbsenceRequestUpdateErrorMessage":"The request could not be updated. Please contact your administrator.",
                                "AbsenceRequestUpdateErrorTitle":"Error",
                                "CalendarUpdateErrorMessage":"Updating the engineer calendar failed,
                                please try again or contact your administrator.",
                                "CalendarUpdateErrorTitle":"Error",
                                "SuccessMessageCancelledTitle":"Success",
                                "SuccessMessageCancelledMessage":"Succesfully cancelled the request for ",
                                "SuccessMessageCancelledButtonAccept":"OK",
                                "SuccesMessageApprovedTitle":"Success",
                                "SuccesMessageApprovedMessage":"Approved the absence request for ",
                                "SuccesMessageApprovedButtonAccept":"OK",
                                "SuccesMessageCancelledIcon":"calendar",
                                "SuccesMessageApprovedIcon":"calendar",
                                "SXPFailedErrorMessage":"The server replied with an error,
                                please try again or contact your administrator. Details: ",
                                "SXPFailedErrorTitle":"Error",
                                "CancelledStatusValue": "Cancelled",
                                "ApprovedStatusValue": "Approved",
                                "PendingStatusValue": "Pending",
                                "NewStatusValue": "New",
                                "CancelRequestStatusValue": "Cancel request"}
     * 
     * 
     * @version 1.0
     * @author  Huub Goltstein
     * @created 2021-05-11
     */
    define(['modules/platform/platformModule'], function () {
        angular.module('platformModule').controller('formApproveAbsencesController',
            ['$scope', 'w6serverServices', 'w6dialogService',
                function ($scope, w6serverServices, w6dialogService) {
                    const SXP_CONTENT_TYPE = 'application/xml';
                    var data = {};
                    MSG_HEADER_ERROR = "Error";
                    // Check if the settings are present and are set
                    if (typeof $scope.additionalParameters === 'undefined' || $scope.additionalParameters.trim().length === 0) {

                        // Show user an error message
                        var oOptions = {};
                        oOptions.title = 'Settings Error';
                        oOptions.body = 'The custom settings for the button were not available. Please contact a system administrator.';
                        oOptions.buttons = [{
                            label: 'OK',
                            kind: 'primary'
                        }];

                        w6dialogService.error(oOptions);
                        return;
                    }
                    // The settings are set by JSON data, these are now set as string, so they need to be parsed to be used as an object
                    $scope.Settings = JSON.parse($scope.additionalParameters);
                    
                    loadEngineer($scope.object.Engineer.Key); // loading of additional values required for functioning.
                    loadStatusOptions();
                    loadAbsenceTypes();      

                    // Event handler for the button click to accept the AbsenceRequest.
                    $scope.onAcceptButtonPress = function () {
                        calendarKey = data.Engineer.Calendar.Key;
                        status = 'NonWork';
                        nonAvailabilityType = getNonAvailabilityType($scope.object.AbsenceType.Key);

                        if (nonAvailabilityType.Key === -1) {
                            if ($scope.object.AbsencePeriod['@DisplayString'] === 'Hele dage') {
                                //it should go from 0:00 to 23:59 and otherwise take over the times from the request.
                                startDate = $scope.object.StartDate;
                                finishDate = dateToEndOfDay($scope.object.FinishDate);
                            } else {
                                startDate = $scope.object.StartDate;
                                finishDate = $scope.object.FinishDate;
                            }
                            updateCalendar(calendarKey, startDate, finishDate, status).then(
                                function(success) { updateAbsenceRequestMeterStatus($scope.Settings.ApprovedStatusValue); }                  
                            ).then(
                                function(success) { succesMessageApproved(); }
                            )
                        }
                        else { // If not an Interval, change the status to trigger a generic event.
                            updateAbsenceRequestMeterStatus($scope.Settings.ApprovedStatusValue).then(
                                function(success) { succesMessageApproved(); }
                            );
                        }
                    };

                    // Event handler for the button click to Cancel an approved AbsenceRequest.
                    $scope.onCancelButtonPress = function () {

                        calendarKey = data.Engineer.Calendar.Key;
                        status = 'Undefined'; // method of "deleting" an interval.
                        nonAvailabilityType = getNonAvailabilityType($scope.object.AbsenceType.Key);

                        if (nonAvailabilityType.Key === -1) {

                            if ($scope.object.AbsencePeriod['@DisplayString'] === 'Hele dage') {
                                //it should go from 0:00 to 23:59 and otherwise take over the times from the request.
                                startDate = $scope.object.StartDate;
                                finishDate = dateToEndOfDay($scope.object.FinishDate);
                            } else {
                                startDate = $scope.object.StartDate;
                                finishDate = $scope.object.FinishDate;
                            }
                            updateCalendar(calendarKey, startDate, finishDate, status).then(
                                function(success) { updateAbsenceRequestMeterStatus($scope.Settings.CancelledStatusValue); }
                            ).then(
                                function(success) { succesMessageCancelled(); },
                            )
                        }
                        else { // should trigger a generic event to remove an N/A.
                            updateAbsenceRequestMeterStatus($scope.Settings.CancelledStatusValue).then(
                                function(success) { succesMessageCancelled(); }
                            )
                        }
                    }

                    /*
                    * Function which replaces the 00:00:00 default to 23:59:00 at the end of a date string.
                    */
                    dateToEndOfDay = function (date) {
                        return date.slice(0, -8) + "23:59:00";
                    };

                    /*
                    * Load the Engineer.
                    */
                    function loadEngineer(engineerKey) {

                        w6serverServices.getObject('Engineer', engineerKey, false).$promise.then(
                            function (Engineer) {
                                data.Engineer = Engineer;
                            }
                        );
                    }

                    /*
                    * Loads the possible statuses.
                    */
                    function loadStatusOptions() {
                        w6serverServices.getObject('AbsenceRequestStatus', false).$promise.then(
                            function (statusOptions) {
                                data.StatusOptions = statusOptions;
                            }
                        );
                    }

                    /*
                    * Loads the corresponding AbsenceType dictionary to retrieve the NonAvailabilityType
                    */
                    function loadAbsenceTypes() {
                        w6serverServices.getObject('AbsenceType', false).$promise.then(
                            function (AbsenceTypes) {
                                data.AbsenceTypes = AbsenceTypes;
                            }
                        );
                    }

                    /*
                    * Retrieves the corresponding NonAvailabilityType from the AbsenceType by key.
                    */
                    function getNonAvailabilityType(absenceTypeKey){
                        for(var i in data.AbsenceTypes){
                            if (data.AbsenceTypes[i].Key == absenceTypeKey){
                                return data.AbsenceTypes[i].NonAvailabilityType;
                            }
                        }
                        
                    }

                    /*
                    * Updates the AbsenceRequestMeter's Status, displays an error message if the call fails.
                    */
                    function updateAbsenceRequestMeterStatus(status) {

                        statusObj = statusToObj(status);

                        var AbsenceRequestMeter = {
                            "@objectType": "AbsenceRequestMeter",
                            Key: $scope.object.Key,
                            AbsenceRequestStatus: {
                                Key : statusObj.Key
                            }
                        }
                        var request = w6serverServices.updateObject('AbsenceRequestMeter', AbsenceRequestMeter, false);
                        request.$promise.then(
                            function (data) {
                                return;
                            },
                            function (error) {
                                // Show user an error message
                                var oOptions = {};
                                oOptions.title = $scope.Settings.AbsenceRequestUpdateErrorTitle;
                                oOptions.body = $scope.Settings.AbsenceRequestUpdateErrorMessage;
                                oOptions.buttons = [{
                                    label: 'OK',
                                    kind: 'primary'
                                }];
                                w6dialogService.error(oOptions);
                                return error;
                            }
                        );
                        return request.$promise;
                    }

                    /*
                    * Retrieves the desired status Obj by name
                    */
                    function statusToObj(status) {
                        for (i in data.StatusOptions) {
                            statusObj = data.StatusOptions[i];
                            if (statusObj.Name == status) {
                                return statusObj;
                            }
                        }
                    }

                    /* 
                    * Function to show the success dialog when the AbsenceRequest has been succesfully cancelled.
                    */
                    function succesMessageCancelled() {
                        var options = {}
                        options.title = $scope.Settings.SuccessMessageCancelledTitle;
                        options.body = $scope.Settings.SuccessMessageCancelledMessage + $scope.object.Engineer['@DisplayString'];
                        options.icon = $scope.Settings.SuccesMessageCancelledIcon;
                        options.buttons = [
                            { label: $scope.Settings.SuccessMessageCancelledButtonAccept, kind: 'primary'}
                        ];
                        options.afterClosed = function() {
                            location.reload();
                        }
                        w6dialogService.show(options);
                    }

                    /* 
                    * Function to show the success dialog when the AbsenceRequest has been succesfully approved.
                    */
                    function succesMessageApproved(){
                        var options = {}
                        options.title = $scope.Settings.SuccesMessageApprovedTitle;
                        options.body = $scope.Settings.SuccesMessageApprovedMessage  + $scope.object.Engineer['@DisplayString'];
                        options.icon = $scope.Settings.SuccesMessageApprovedIcon;
                        options.buttons = [
                            { label: $scope.Settings.SuccesMessageApprovedButtonAccept, kind: 'primary'} 
                        ];
                        options.afterClosed = function() {
                            location.reload();
                        }
                        w6dialogService.show(options);
                    }

                    function updateCalendar(calendarKey, startDate, finishDate, status) {
                        // Build SXP Message
                        var message = '<SXPCalendarUpdate Revision="7.5.0">' +
                            '<Calendar>' +
                            '<Key>' + calendarKey + '</Key>' +
                            '<YearlyLevel>' +
                            '<YearlyInterval>' +
                            '<TimeInterval>' +
                            '<Start>' + startDate + '</Start>' +
                            '<Finish>' + finishDate + '</Finish>' +
                            '</TimeInterval>' +
                            '<Status>' + status + '</Status>' +
                            '<OverwriteExisting>false</OverwriteExisting>' +
                            '</YearlyInterval>' +
                            '</YearlyLevel>' +
                            '</Calendar>' +
                            '<OverwriteExisting>false</OverwriteExisting>' +
                            '</SXPCalendarUpdate>';

                        var request = w6serverServices.sendSXP(message).then(
                            function (sxpResponse) {
                                // Parse the XML response into a DOM object, Click returns a success but with an Error if received and processed but invalid request.
                                var parser = new DOMParser();
                                var document = parser.parseFromString(sxpResponse.data, 'application/xml');
                                if (document.getElementsByTagName('ErrorDescription').length > 0) {
                                    var options = {};
                                    options.title = $scope.Settings.SXPFailedErrorTitle;
                                    options.body = $scope.Settings.SXPFailedErrorMessage + document.getElementsByTagName('ErrorDescription')[0].innerHTML;
                                    w6dialogService.error(options);
                                    console.log('Error on SXPExtendedTaskGetAppointments. Request: ' + message + '; Response: ' + sxpResponse);
                                    return Promise.reject('Calendar failure'); // break the chain of .then()'s
                                }
                            },
                            function (response) {
                                var options = {};
                                options.title = $scope.Settings.CalendarUpdateErrorTitle;
                                options.body = $scope.Settings.CalendarUpdateErrorMessage;
                                w6dialogService.error(options);
                                console.log('Error on SXPCalendarUpdate. Request: ' + message + '; Response: ' + response);
                            }
                        );
                        return request;
                    }
                }]);
    });